(function() {
  'use strict';

  angular
    .module('app', ['ngAnimate', 'ngTouch', 'ngSanitize', 'ngAria', 'ui.router', 'mgcrea.ngStrap', 'btford.markdown']);

})();
